package com.wolfking.back.core.bean;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyTable;

/**
 * 机构的实体
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日下午12:35:49
 * @版权 归wolfking所有
 */
@MyTable("sys_office")
public class Office extends ParentEntity {

	private static final long serialVersionUID = 5332467094071516552L;
	@MyColumn
	private String name; // 机构名称
	@MyColumn
	private Integer sort; // 排序
	@MyColumn
	private String code; // 机构编码
	@MyColumn
	private String type; // 机构类型（1：公司；2：部门；3：小组）
	@MyColumn
	private String grade; // 机构等级（1：一级；2：二级；3：三级；4：四级）
	@MyColumn
	private String address; // 联系地址
	@MyColumn("zip_code")
	private String zipCode; // 邮政编码
	@MyColumn
	private String master; // 负责人
	@MyColumn
	private String phone; // 电话
	@MyColumn
	private String fax; // 传真
	@MyColumn
	private String email; // 邮箱
	@MyColumn
	private String useable;// 是否可用
	@MyColumn("PRIMARY_PERSON")
	private String primaryPerson;// 主负责人
	@MyColumn("DEPUTY_PERSON")
	private String deputyPerson;// 副负责人
	@MyColumn("area_id")
	private String areaId;

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return sort
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * @param sort
	 *            要设置的 sort
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            要设置的 code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            要设置的 type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade
	 *            要设置的 grade
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            要设置的 address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            要设置的 zipCode
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return master
	 */
	public String getMaster() {
		return master;
	}

	/**
	 * @param master
	 *            要设置的 master
	 */
	public void setMaster(String master) {
		this.master = master;
	}

	/**
	 * @return phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            要设置的 phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            要设置的 fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            要设置的 email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return useable
	 */
	public String getUseable() {
		return useable;
	}

	/**
	 * @param useable
	 *            要设置的 useable
	 */
	public void setUseable(String useable) {
		this.useable = useable;
	}

	/**
	 * @return primaryPerson
	 */
	public String getPrimaryPerson() {
		return primaryPerson;
	}

	/**
	 * @param primaryPerson
	 *            要设置的 primaryPerson
	 */
	public void setPrimaryPerson(String primaryPerson) {
		this.primaryPerson = primaryPerson;
	}

	/**
	 * @return deputyPerson
	 */
	public String getDeputyPerson() {
		return deputyPerson;
	}

	/**
	 * @param deputyPerson
	 *            要设置的 deputyPerson
	 */
	public void setDeputyPerson(String deputyPerson) {
		this.deputyPerson = deputyPerson;
	}

	/**
	 * @return areaId
	 */
	public String getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId
	 *            要设置的 areaId
	 */
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

}
