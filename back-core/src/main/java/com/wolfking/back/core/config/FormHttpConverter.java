/**
 * 
 */
package com.wolfking.back.core.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Component;

/**
 * FormHttpMessageConverter
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日下午2:16:39
 * @版权 归wolfking所有
 */
@Component
public class FormHttpConverter extends FormHttpMessageConverter {
	public FormHttpConverter() {
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.APPLICATION_FORM_URLENCODED);
		setSupportedMediaTypes(mediaTypeList);
	}
}
