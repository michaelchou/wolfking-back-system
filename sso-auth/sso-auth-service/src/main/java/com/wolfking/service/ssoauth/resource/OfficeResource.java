package com.wolfking.service.ssoauth.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Office;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.OfficeService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/office")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/office", description = "机构的服务", produces = MediaType.APPLICATION_JSON)
public class OfficeResource {

	@Autowired
	private OfficeService officeService;

	@POST
	@ApiOperation(value = "机构添加接口", httpMethod = "POST", notes = "机构添加接口", response = Boolean.class)
	public Response addOffice(@RequestBody Office office) {
		return ResponseUtil.okResponse(officeService.add(office));
	}

	@PUT
	@ApiOperation(value = "机构修改接口", httpMethod = "PUT", notes = "机构修改接口", response = Boolean.class)
	public Response updateOffice(@RequestBody Office office) {
		return ResponseUtil.okResponse(officeService.update(office));
	}

	@GET
	@ApiOperation(value = "查询所有的机构", httpMethod = "GET", notes = "查询所有的机构", response = Office.class, responseContainer = "List")
	public Response getAllOffice() {
		return ResponseUtil.okResponse(officeService.findAll());
	}

	@GET
	@Path("/findAllChild/{id}")
	@ApiOperation(value = "查询所有的子机构", httpMethod = "GET", notes = "查询所有的子机构", response = Office.class, responseContainer = "List")
	public Response getAllChildOffice(@PathParam(value = "id") String id) {
		return ResponseUtil.okResponse(officeService.getAllChild(id));
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询机构接口", httpMethod = "GET", notes = "根据ID查询机构接口", response = Office.class)
	public Response getOffice(@PathParam("id") @ApiParam(required = true, name = "id", value = "机构ID") String id) {
		Office office = officeService.getById(id);
		return ResponseUtil.okResponse(office);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除机构接口", httpMethod = "DELETE", notes = "根据ID删除机构接口", response = Boolean.class)
	public Response deleteOffice(@PathParam("id") @ApiParam(required = true, name = "id", value = "机构ID") String id) {
		return ResponseUtil.okResponse(officeService.deleteById(id));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询机构信息", httpMethod = "POST", notes = "分页查询机构信息", response = Office.class, responseContainer = "PageInfo")
	public Response pageOffice(@RequestBody Office office,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<Office> pageInfo = officeService.pageVague(office, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}

}
