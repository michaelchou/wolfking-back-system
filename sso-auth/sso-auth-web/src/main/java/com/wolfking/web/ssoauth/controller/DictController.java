package com.wolfking.web.ssoauth.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wolfking.back.core.annotation.ssoauth.Sign;
import com.wolfking.back.core.bean.Dict;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.config.SsoConfig;
import com.wolfking.back.core.feignclient.DictFeignClient;
import com.wolfking.back.core.util.DataEntityUtil;

/**
 * 字典的controller
 * <P>
 * 字典的增删查改
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月26日下午1:09:59
 * @版权 归wolfking所有
 */
@Controller
@RequestMapping("/dict")
public class DictController {

	@Autowired
	private DictFeignClient dictFeignClient;

	@Autowired
	private SsoConfig ssoConfig;

	/**
	 * 字典的列表页，包括模糊查询
	 * <p>
	 * 
	 * @param model
	 * @param dict
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@Sign(code="sys:dict:view")
	@RequestMapping(value = "/list", method = { RequestMethod.GET, RequestMethod.POST })
	public String list(Model model, Dict dict, @RequestParam(defaultValue = "1") int pageNum,
			@RequestParam(defaultValue = "10") int pageSize) {
		PageInfo<Dict> page = dictFeignClient.pageDict(dict, pageNum, pageSize).getBody();
		List<String> allType = dictFeignClient.getAllType().getBody();
		model.addAttribute("page", page);
		model.addAttribute("allType", allType);
		model.addAttribute("dict", dict);
		return "dict/list";
	}

	/**
	 * 字典的编辑页面
	 * 
	 * @param model
	 * @param dict
	 * @return
	 */
	@Sign(code="sys:dict:view")
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Model model, Dict dict) {
		if (StringUtils.isNotBlank(dict.getId()))
			dict = dictFeignClient.getDict(dict.getId()).getBody();
		model.addAttribute("dict", dict);
		return "dict/edit";
	}

	/**
	 * 字典的修改
	 * @param dict
	 * @param user
	 * @param redirectAttributes
	 * @return
	 */
	@Sign(code="sys:dict:edit")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String edits(Dict dict, User user, RedirectAttributes redirectAttributes) {
		DataEntityUtil.assemblyDataEntity(dict, user);
		if (StringUtils.isNotBlank(dict.getId())) {
			dictFeignClient.updateDict(dict);
			redirectAttributes.addFlashAttribute("message", "修改字典成功");
		} else {
			dictFeignClient.addDict(dict);
			redirectAttributes.addFlashAttribute("message", "保存字典成功");
		}
		return "redirect:" + ssoConfig.getSelfRootUrl() + "/dict/list";
	}

	/**
	 * 删除字典值
	 * 
	 * @param id
	 * @return
	 */
	@Sign(code="sys:dict:edit")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(@RequestParam String id) {
		if (StringUtils.isNotBlank(id))
			dictFeignClient.deleteDict(id);
		return "redirect:" + ssoConfig.getSelfRootUrl() + "/dict/list";
	}

}
