package com.wolfking.web.ssoauth.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wolfking.back.core.annotation.ssoauth.Sign;
import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.service.SsoUserService;

/**
 * 登录的controller
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日下午5:50:23
 * @版权 归wolfking所有
 */
@Controller
public class MainController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private SsoUserService ssoUserService;

	@Sign
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String main(Model model, User user, HttpServletRequest request) {
		logger.info("登录用户的tokenId is {}", ssoUserService.getTokenId(request));
		logger.info("user is : {}", user);
		List<Menu> menuList = ssoUserService.getUserMenus(user.getId());
		logger.info("menuList is : {}", menuList);
		return "main";
	}
}
