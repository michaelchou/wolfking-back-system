/**
 * 
 */
package com.wolfking.web.ssoauth.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.wolfking.back.core.annotation.ssoauth.Sign;
import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.config.SsoConfig;
import com.wolfking.back.core.feignclient.MenuFeignClient;
import com.wolfking.back.core.util.DataEntityUtil;

/**
 * 菜单的controller
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月2日上午11:36:02
 * @版权 归wolfking所有
 */
@Controller
@RequestMapping("/menu")
public class MenuController {
	@Autowired
	private MenuFeignClient menuFeignClient;
	@Autowired
	private SsoConfig ssoConfig;

	@Sign
	@RequestMapping(value = "/list", method = { RequestMethod.GET, RequestMethod.POST })
	public String list(Model model) {
		List<Menu> sourcelist = menuFeignClient.getAllMenu().getBody();
		List<Menu> list = Lists.newArrayList();
		sortList(list, sourcelist, "1", true);
		model.addAttribute("list", list);
		return "menu/list";
	}

	/**
	 * 字典的编辑页面
	 * 
	 * @param model
	 * @param dict
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Model model, Menu menu) {
		if (StringUtils.isNotBlank(menu.getId()))
			menu = menuFeignClient.getMenu(menu.getId()).getBody();
		model.addAttribute("menu", menu);
		List<Menu> list = menuFeignClient.getAllMenu().getBody();
		model.addAttribute("list", list);
		return "menu/edit";
	}

	/**
	 * 字典的保存或者修改
	 * 
	 * @param model
	 * @param dict
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String edits(Menu menu, User user,RedirectAttributes redirectAttributes) {
		DataEntityUtil.assemblyDataEntity(menu, user);
		if (StringUtils.isNotBlank(menu.getId())){
			menuFeignClient.updateMenu(menu);
			redirectAttributes.addFlashAttribute("message", "修改菜单成功");
		}else {
			menuFeignClient.addMenu(menu);
			redirectAttributes.addFlashAttribute("message", "添加菜单成功");
		}
		return "redirect:" + ssoConfig.getSelfRootUrl() + "/menu/list";
	}

	/**
	 * 删除字典值
	 * 
	 * @param id
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(@RequestParam String id) {
		if (StringUtils.isNotBlank(id))
			menuFeignClient.deleteMenu(id);
		return "redirect:" + ssoConfig.getSelfRootUrl() + "/menu/list";
	}

	private void sortList(List<Menu> list, List<Menu> sourcelist, String parentId, boolean cascade) {
		for (int i = 0; i < sourcelist.size(); i++) {
			Menu e = sourcelist.get(i);
			if (parentId.equals(e.getParentId())) {
				list.add(e);
				if (cascade) {
					// 判断是否还有子节点, 有则继续获取子节点
					for (int j = 0; j < sourcelist.size(); j++) {
						Menu child = sourcelist.get(j);
						if (e.getId().equals(child.getParentId())) {
							sortList(list, sourcelist, e.getId(), true);
							break;
						}
					}
				}
			}
		}
	}
}
